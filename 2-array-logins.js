// 1. Find all people who are Agender
// 2. Split their IP address into their components eg. 111.139.161.143 has components [111, 139, 161, 143]
// 3. Find the sum of all the second components of the ip addresses.
// 3. Find the sum of all the fourth components of the ip addresses.
// 4. Compute the full name of each person and store it in a new key (full_name or something) for each person.
// 5. Filter out all the .org emails
// 6. Calculate how many .org, .au, .com emails are there
// 7. Sort the data in descending order of first name



// Importing dataset

let people = require("./dataset");


function findAgender(people) {
    return people.filter((person) => {
        return person.gender === "Agender";
    })

}

function splitIPAddresses(people) {
    return people.map((person) => {
        return person.ip_address.split(".").map((number) => {
            return +number;
        });
    })
}



function sumOfSecondAndFourthComponent(people, index) {
    return people.map((person) => {
        return person.ip_address.split(".");
    }).reduce((acc, curr) => {
        return acc + +curr[index - 1];
    }, 0)
}




function computeFullName(people) {
    return people.map((person) => {
        person["fullname"] = `${person.first_name} ${person.last_name}`
        return person;
    })
}



function filterOrgEmails(people) {
    return people.filter((person) => {
        return person.email.endsWith(".org");
    })
}


function getCountOfEmails() {
    let resultObject = {};
    arguments[1].forEach((extension) => {
        resultObject[extension] = 0;
    })
    return arguments[0].reduce((acc, person) => {
        Object.keys(resultObject).forEach((extension) => {
            if (person.email.endsWith(extension)) {
                acc[extension] += 1;
            }
        })
        return acc;
    }, resultObject)
}

// example 
// console.log(getCountOfEmails(people,[".org",".au",".com"]));

function sortDescending(people) {
    return people.sort((first, second) => {
        return second.first_name.localeCompare(first.first_name);
    })
}

